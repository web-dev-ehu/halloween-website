import { AppStore } from "./app.store.ts";
import "./style.css";
import {
  TicketSection,
  HeroSection,
  JoinUsSection,
  GallerySection,
} from "./sections";
import { FooterSection } from "./sections/footer-section/footer-section.ts";
import { NavigationService } from "./shared/navigation.service.ts";

export class App {
  store: AppStore;
  host: HTMLDivElement = document.querySelector<HTMLDivElement>("#app")!;

  constructor() {
    this.store = new AppStore();
  }

  public render(): void {
    this.host.innerHTML = `
            <main>
                <app-header>
                    <app-button id="tickets" slot="action" data-i18n-label="header.button.text" width="140px" mode="main"></app-button>
                </app-header>
                <div class="section-container"></div>
            </main>
        `;

    const sectionContainer = this.host.querySelector(".section-container");
    const button = this.host.querySelector("app-button#tickets");

    button!.addEventListener(
      "click",
      NavigationService.navigate("ticket-section"),
    );

    const heroSection: HTMLElement = HeroSection.render();
    const gallerySection: HTMLElement = GallerySection.render();
    const joinUsSection: HTMLElement = JoinUsSection.render();
    const ticketSection: HTMLElement = TicketSection.render();
    const footerSection: HTMLElement = FooterSection.render();

    sectionContainer!.append(heroSection);
    sectionContainer!.append(gallerySection);
    sectionContainer!.append(joinUsSection);
    sectionContainer!.append(ticketSection);
    sectionContainer!.append(footerSection);
  }
}
