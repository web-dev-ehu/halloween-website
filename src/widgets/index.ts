import { AppGalleryWidget } from "./app-gallery/gallery.ts";
import { AppHeader } from "./app-header/app-header.ts";

export function declareWidgets() {
  window.customElements.define("app-gallery", AppGalleryWidget);
  window.customElements.define("app-header", AppHeader);
}
