import STYLES from "./gallery.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";

const TEMPLATE = `
    <div class="gallery-container"></div>
`;

export class AppGalleryWidget extends HTMLDirectionElement {
  constructor() {
    super(["images"], TEMPLATE, STYLES);
  }

  override _render() {
    const imageContainer = this.node.querySelector(".gallery-container");

    // console.log(imageContainer);
    if (imageContainer) {
      imageContainer.innerHTML = "";
      // console.log(this);
      const images = this.getAttribute("images");

      if (!images) {
        console.error("No images passed to AppGalleryWidget.");
        return;
      }

      images.split(",").forEach((imageUrl) => {
        const img = document.createElement("img");
        img.src = imageUrl.trim();
        imageContainer.appendChild(img);
      });
    }
  }
}
