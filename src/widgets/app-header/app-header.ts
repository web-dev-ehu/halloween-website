import STYLES from "./app-header.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";
import { app } from "../../main.ts";
import { NavigationService } from "../../shared/navigation.service.ts";

const TEMPLATE = `
    <div class="header" data-dir>
        <div class="header__logo" data-i18n="header.logo"></div>
        <div class="header__menu">
            <div class="header__menu__item" id="hero" data-i18n="menu.home"></div>
            <div class="header__menu__item" id="gallery" data-i18n="menu.gallery"></div>
            <div class="header__menu__item" id="join-us" data-i18n="menu.about"></div>
            <div class="header__menu__item" id="ticket" data-i18n="menu.reservation"></div>
            <div class="header__menu__item" id="footer" data-i18n="menu.contacts"></div>
        </div>
        <div class="header__locales">
            <div class="header__locale" data-i18n="menu.locales.en" data-locale="EN"></div>
            <div class="header__locale" data-i18n="menu.locales.ar" data-locale="AR"></div>
        </div>
        <slot name="action"></slot>
    </div>
`;

export class AppHeader extends HTMLDirectionElement {
  constructor() {
    super([], TEMPLATE, STYLES);
  }

  override _render() {
    this.node.querySelectorAll(".header__locale").forEach((x) => {
      x.addEventListener("click", () => {
        app.store.setLocale(x.getAttribute("data-locale")!);
      });
    });

    this.node.querySelectorAll(".header__menu__item").forEach((x) => {
      x.addEventListener(
        "click",
        NavigationService.navigate(`${x.id}-section`),
      );
    });
  }
}
