import "./hero-section.css";
import calendar from "../../../assets/calendar.svg";
import arrowImg from "../../../assets/arrow-down.svg";
import { NavigationService } from "../../shared/navigation.service.ts";

export class HeroSection {
  public static render() {
    const section = document.createElement("section");
    section.id = "hero-section";
    section.innerHTML = `
            <div class="date__container" data-dir>
                <img src="${calendar}" alt="">
                <span class="date__span" data-i18n="hero-section.date"></span>
            </div>
            <h1 class="header" data-i18n="hero-section.header"></h1>
            <img class="arrow" src="${arrowImg}" alt="">
        `;

    const arrow = section.querySelector(".arrow");

    arrow?.addEventListener(
      "click",
      NavigationService.navigate("ticket-section"),
    );

    return section;
  }
}
