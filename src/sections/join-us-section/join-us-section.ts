import "./join-us-section.css";
import arrowLeftImg from "../../../assets/arrow-left.svg";
import arrowRightImg from "../../../assets/arrow-right.svg";

export class JoinUsSection {
  public static render() {
    const section = document.createElement("section");
    section.id = "join-us-section";
    section.innerHTML = `
            <div class="section__container">
                <div class="heading__container">
                    <h1 class="header-1" data-i18n="join-us.header-1"></h1>
                    <div class="arrows__container">
                        <img src="${arrowRightImg}" alt="arrowRight-img">
                        <h1 class="header-2" data-i18n="join-us.header-2"></h1>
                        <img src="${arrowLeftImg}" alt="arrowLeft-img">
                    </div>
                </div>   
                <span class="text__span" data-i18n="join-us.span"></span>
                <app-ticket datetime="ticket.datetime" location="ticket.location"></app-ticket>
                <div class="checkbox__container">
                    <app-checkbox data-i18n-label="checkbox1.label"></app-checkbox>
                    <app-checkbox data-i18n-label="checkbox2.label"></app-checkbox>
                    <app-checkbox data-i18n-label="checkbox3.label"></app-checkbox>
                    <app-checkbox data-i18n-label="checkbox4.label"></app-checkbox>
                    <app-checkbox data-i18n-label="checkbox5.label"></app-checkbox>
                </div>
            </div> 
        `;

    return section;
  }
}
