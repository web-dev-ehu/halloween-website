import "./gallery-section.css";

import galleryImage1 from "../../../assets/gallery-pic-1.png";
import galleryImage2 from "../../../assets/gallery-pic-2.png";
import galleryImage3 from "../../../assets/gallery-pic-3.png";
import galleryImage4 from "../../../assets/gallery-pic-4.png";
import galleryImage5 from "../../../assets/gallery-pic-5.png";
import galleryImage6 from "../../../assets/gallery-pic-6.png";
import galleryImage7 from "../../../assets/gallery-pic-7.png";
import galleryImage8 from "../../../assets/gallery-pic-8.png";
import galleryImage9 from "../../../assets/gallery-pic-9.png";
import galleryImage10 from "../../../assets/gallery-pic-10.png";

export class GallerySection {
  public static render() {
    const galleryImages = [
      galleryImage1,
      galleryImage2,
      galleryImage3,
      galleryImage4,
      galleryImage5,
      galleryImage6,
      galleryImage7,
      galleryImage8,
      galleryImage9,
      galleryImage10,
    ];
    const section = document.createElement("section");
    section.id = "gallery-section";
    section.innerHTML = `
            <h1 class="header" data-i18n="gallery.header"></h1>
            <app-gallery images="${galleryImages.join(",")}"></app-gallery>
        `;

    return section;
  }
}
