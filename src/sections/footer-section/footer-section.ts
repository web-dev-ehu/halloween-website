import "./footer-section.css";
import footerBgImg from "../../../assets/footer-grass.png";
import behanceImg from "../../../assets/behance.png";
import figmaImg from "../../../assets/Figma.png";
import linkedInImg from "../../../assets/LinkedIn.png";
import instagramImg from "../../../assets/instagram.png";
import youtubeImg from "../../../assets/youtube.png";

export class FooterSection {
  public static render() {
    const section = document.createElement("section");
    section.id = "footer-section";
    section.innerHTML = `
        <img src="${footerBgImg}" alt="footer-bg" class="background">
        <div class="container">
            <h1 class="phone" data-i18n="footer.phone"></h1>
            <div class="social-container">
                <img src="${behanceImg}" alt="">
                <img src="${figmaImg}" alt="">
                <img src="${linkedInImg}" alt="">
                <img src="${instagramImg}" alt="">
                <img src="${youtubeImg}" alt="">
            </div>
        </div>
    `;

    return section;
  }
}
