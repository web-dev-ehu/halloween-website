import "./ticket-section.css";

export class TicketSection {
  public static render() {
    const section = document.createElement("section");
    section.id = "ticket-section";
    section.innerHTML = `
            <h1 data-i18n="tickets.heading"></h1>
            <div class="price-plans-container">
                <app-price-card 
                    mode="gray" 
                    data-i18n-heading="card1.heading" 
                    data-i18n-price="card1.price" 
                    data-i18n-list="card1.list.item-"
                >        
                    <app-button slot="action" data-i18n-label="card.button.text" width="240px" mode="main"></app-button>
                </app-price-card>
                <app-price-card 
                    mode="main" 
                    data-i18n-heading="card2.heading" 
                    data-i18n-price="card2.price"
                    data-i18n-list="card2.list.item-"
                >
                    <app-button slot="action" data-i18n-label="card.button.text" width="240px" mode="main"></app-button>
                </app-price-card>
                <app-price-card 
                    mode="gray" 
                    data-i18n-heading="card3.heading" 
                    data-i18n-price="card3.price" 
                    data-i18n-list="card3.list.item-"
                >
                    <app-button slot="action" data-i18n-label="card.button.text" width="240px" mode="main"></app-button>
                </app-price-card>
            </div>
        `;

    return section;
  }
}
