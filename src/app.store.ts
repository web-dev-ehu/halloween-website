import { localeList } from "./app.config.ts";

export type TLocale = {
  name: string;
  dir: "ltr" | "rtl";
  isActive: boolean;
  translations: { [key: string]: string };
};

export class AppStore {
  public locales: TLocale[] = localeList;

  public get activeLocale(): TLocale {
    const active = this.locales.find((x) => x.isActive);
    if (active) {
      return active;
    }
    return this.locales[0];
  }

  public setLocale(localeName: string) {
    const locale = this.locales.find((x) => x.name === localeName);
    if (locale) {
      this.locales = this.locales.map((x) => {
        x.isActive = false;
        return x;
      });
      locale.isActive = true;
      this.translate();
    }
  }

  public translate() {
    const locale = this.activeLocale;
    const searchPlaces = [
      document,
      ...[...document.querySelectorAll("[data-uuid]")].map(
        (x) => x.shadowRoot!,
      ),
    ];

    searchPlaces.forEach((place) => {
      const directionContainers = place.querySelectorAll("[data-dir]");
      directionContainers.forEach((element) => {
        element.setAttribute("data-dir", locale.dir);
      });

      const languageContainers = place.querySelectorAll("[data-i18n]");
      languageContainers.forEach((element) => {
        const i18nKey = element.getAttribute("data-i18n");

        if (!i18nKey || !i18nKey.length) {
          console.error(
            `Translation key (data-i18n) was not provided in `,
            element,
          );
          return;
        }

        element.innerHTML = locale.translations[i18nKey];
      });
    });
  }
}
