import "./style.css";
import { declareComponents } from "./components";
import { declareWidgets } from "./widgets";

import { App } from "./app.ts";

declareComponents();
declareWidgets();
export const app: App = new App();

document.addEventListener("DOMContentLoaded", () => {
  app.render();
  app.store.setLocale(app.store.activeLocale.name);
});
