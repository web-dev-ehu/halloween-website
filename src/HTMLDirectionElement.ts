import { v4 as uuid } from "uuid";

export class HTMLDirectionElement extends HTMLElement {
  public observedAttributes: string[];
  public uuid: string;
  public connectedCallback = this._render;
  public attributeChangedCallback = this._render;
  public node: HTMLElement;

  constructor(inputs: string[], template: string, styles: string) {
    super();
    this.attachShadow({ mode: "open" });
    this.observedAttributes = [...inputs];
    this.uuid = uuid();
    this.setAttribute("data-uuid", this.uuid);

    const el: HTMLTemplateElement = document.createElement("template");
    el.innerHTML = `<style>${styles}</style>` + template;
    this.node = <HTMLElement>el.content.cloneNode(true);
    this._render();
    this.shadowRoot!.appendChild(this.node);
  }

  protected _render() {
    throw new SyntaxError(`Component method _render is not implemented.`);
  }

  public setDataKey(selector: string, key: string, input: string) {
    this.node
      .querySelector(selector)!
      .setAttribute(key, this.getAttribute(input) || "ERROR");
  }
}
