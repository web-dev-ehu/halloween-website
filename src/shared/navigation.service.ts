export class NavigationService {
  public static navigate(id: string) {
    return function () {
      const element = document.getElementById(id);
      if (element) {
        element!.scrollIntoView({ behavior: "smooth" });
      } else {
        console.error(
          `NAVIGATION_FAILED: No element with id ${id} on the page.`,
        );
      }
    };
  }
}
