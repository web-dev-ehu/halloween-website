import ticketSquareImg from "../../../assets/ticket-square.svg";
import locationImg from "../../../assets/location.svg";
import STYLES from "./app-ticket.component.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";

const TEMPLATE: string = `
    <div class="ticket-component__container" data-dir>
        <img class="ticket__image" src="${ticketSquareImg}" alt="ticket-image">
        <div class="ticket-data__container">
            <div class="ticket-data__datetime"></div>
            <div class="ticket-data__location__container" data-dir>
                <img class="ticket-data__location__image" src="${locationImg}" alt="location-image">
                <div class="ticket-data__location"></div>
</div>
    </div>
</div>
 `;

export class AppTicketComponent extends HTMLDirectionElement {
  constructor() {
    super(["datetime", "location"], TEMPLATE, STYLES);
  }

  override _render() {
    this.setDataKey(".ticket-data__datetime", "data-i18n", "datetime");
    this.setDataKey(".ticket-data__location", "data-i18n", "location");
  }
}
