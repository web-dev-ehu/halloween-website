import STYLES from "./app-price-card.component.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";
import { AppButtonComponent } from "../app-button/app-button.component.ts";

const TEMPLATE: string = `
    <div class="price-card__container" data-dir="rtl">
        <h3 class="price-card__heading"></h3>
        <p class="price-card__price"></p>
        <div class="price-card__program-list">
            <div class="price-card__program-list__item-1"></div>
            <div class="price-card__program-list__item-2"></div>
            <div class="price-card__program-list__item-3"></div>
        </div>
        <slot name="action"></slot>
    </div>
`;

export class AppPriceCardComponent extends HTMLDirectionElement {
  constructor() {
    super(
      [
        "data-i18n-heading",
        "data-i18n-price",
        "data-i18n-button",
        "data-i18n-list",
      ],
      TEMPLATE,
      STYLES,
    );
  }

  reRenderButton(hovered: boolean) {
    const container = this.shadowRoot!.querySelector(".price-card__container")!;
    container.classList.toggle("hovered", hovered);

    const button: AppButtonComponent = this.querySelector("app-button")!;
    const copy: AppButtonComponent = button.cloneNode(
      true,
    ) as AppButtonComponent;

    copy.setAttribute("mode", hovered ? "main" : "gray");
    copy.shadowRoot!.querySelector("button")!.innerText =
      button.shadowRoot!.querySelector("button")!.innerText;

    this.append(copy);
    this.removeChild(button);
  }

  override _render() {
    const cardListAttr = this.getAttribute("data-i18n-list") || "ERROR";

    this.setDataKey(".price-card__heading", "data-i18n", "data-i18n-heading");
    this.setDataKey(".price-card__price", "data-i18n", "data-i18n-price");
    this.node
      .querySelector(".price-card__program-list__item-1")!
      .setAttribute("data-i18n", `${cardListAttr}1`);
    this.node
      .querySelector(".price-card__program-list__item-2")!
      .setAttribute("data-i18n", `${cardListAttr}2`);
    this.node
      .querySelector(".price-card__program-list__item-3")!
      .setAttribute("data-i18n", `${cardListAttr}3`);

    setTimeout(() => {
      this.reRenderButton(false);
      this.addEventListener("mouseenter", () => {
        this.reRenderButton(true);
      });
      this.addEventListener("mouseleave", () => {
        this.reRenderButton(false);
      });
    });
  }
}
