import tickSquare from "../../../assets/tick-square.svg";
import STYLES from "./app-checkbox.component.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";

const TEMPLATE: string = `
    <div class="tick-square__container" data-dir>
        <img class="tick-square__image" src="${tickSquare}" alt="">
        <span class="tick-square__span"></span>
    </div>
`;

export class AppCheckboxComponent extends HTMLDirectionElement {
  constructor() {
    super(["data-i18n-label"], TEMPLATE, STYLES);
  }

  override _render() {
    this.setDataKey(".tick-square__span", "data-i18n", "data-i18n-label");
  }
}
