import STYLES from "./app-button.component.css?inline";
import { HTMLDirectionElement } from "../../HTMLDirectionElement.ts";

const TEMPLATE: string = `
    <button class="btn-component"></button>
`;

export class AppButtonComponent extends HTMLDirectionElement {
  constructor() {
    super(["width", "data-i18n-label", "mode"], TEMPLATE, STYLES);
  }

  override _render() {
    this.node
      .querySelector(".btn-component")!
      .setAttribute("mode", this.getAttribute("mode") || "main");
    (<HTMLButtonElement>this.node.querySelector(".btn-component")).style.width =
      this.getAttribute("width") || "max-content";

    this.setDataKey(".btn-component", "data-i18n", "data-i18n-label");
  }
}
