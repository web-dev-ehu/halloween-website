import { AppButtonComponent } from "./app-button/app-button.component.ts";
import { AppCheckboxComponent } from "./app-checkbox/app-checkbox.component.ts";
import { AppPriceCardComponent } from "./app-price-card/app-price-card.component.ts";
import { AppTicketComponent } from "./app-ticket/app-ticket.component.ts";

export function declareComponents() {
  window.customElements.define("app-button", AppButtonComponent);
  window.customElements.define("app-checkbox", AppCheckboxComponent);
  window.customElements.define("app-price-card", AppPriceCardComponent);
  window.customElements.define("app-ticket", AppTicketComponent);
}
