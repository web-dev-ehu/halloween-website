# Halloween Website

Our Halloween Website project is utilizing Vite, TypeScript, and HTML5 to build a site that is both visually appealing and
functionally robust. Key features include a meticulously implemented design with assets
sourced from Figma and deployment on Gitlab Pages. The site will have interactive sections such as Header,
Hero, Footer, an Images section with hover effects, a Texts section inviting users
to join the Halloween party, and an ECommerce section with dynamic color-changing cards.
Additionally, the project embraces bilingual support (English and Arabic)
and adheres to high standards of code quality, utilizing tools like Prettier and ESLint.

### Useful links

[Figma](<https://www.figma.com/file/Z9i2HiOV3VtvhnqkKBUUon/Free-Halloween-Party-Time-Landing-Page-(Community)?type=design&node-id=0-1&mode=design&t=rp5neXS05UbP5lGk-0>)

### Steps to launch

```shell
npm run
```

### Contributors

[Vlad Tihomirov](https://gitlab.com/vlad.tihomirov@vlad.tihomirov) - Team lead

[Andrei Ramanenka](https://gitlab.com/andrei.ramanenkaa) - Developer

[Voiteshonok](https://gitlab.com/Slav_Voit) - Developer

### Preview

[Gitlab Page](https://halloween-website-web-dev-ehu-1b3530b5a7f09bf407e68ca08746b5dde.gitlab.io/)
